var spaces=[0,0,0,0,0,0,0,0,0,0];
var currentPlayer=1;
var cells;
window.onload=function(){
cells=document.getElementsByTagName("th");
}
function place(position){
if(spaces[position]==0){
	spaces[position]=currentPlayer;
	cells[position].innerHTML=piece();
	if (checkWin()){
			$("#player").html("Player "+ piece()+" wins!!!");
		for (var i=0;i<9;i++){
			cells[i].innerHTML="Player "+piece()+" wins!";
		}	
}
	else{
		if (currentPlayer==1){
			currentPlayer=2;
		}
		else {currentPlayer=1;}
	$("#player").html("Player "+currentPlayer+"'s turn.");
}

}
}

function checkWin(){

return checkRow(0,1,2)||checkRow(3,4,5)||checkRow(6,7,8)||
		 checkRow(0,3,6)||checkRow(1,4,7)||checkRow(2,5,8)||
		 checkRow(0,4,8)||checkRow(2,4,6);
}

function checkRow(a,b,c){
return spaces[a]==spaces[b]&&spaces[a]==spaces[c]&&spaces[a]==currentPlayer;
}

function piece(){
if (currentPlayer==1){
return "X";
}
else {return "O";}
}

function Reset(){
	spaces=[0,0,0,0,0,0,0,0,0,0];
	for (var i=0;i<9;i++){
		cells[i].innerHTML="";
		$("#player").html("Player 1's turn");
		currentPlayer=1;
}
}
