function requestSchedule(){
	var VIds=[];
	var BIds=[];
	var directions=[];
	var destinations=[];
	var delays=[];
	var number = $("#number").val();
	var URL="http://www3.septa.org/hackathon/TransitView/?route="+number+"&callback=?";
	   $.ajax({
		type: "GET",
		url: URL,
		contentType:  "application/json; charset=utf-8",
		data: "{}",
		dataType: "jsonp",
		success: function(msg) {
			console.log(URL);
			var json = msg;
			for(var info in json.bus){
				VIds.push(json.bus[info].VehicleID);
				BIds.push(json.bus[info].BlockID);
				directions.push(json.bus[info].Direction);
				destinations.push(json.bus[info].destination);
				delays.push(json.bus[info].Offset_sec);
			};
			var table=document.getElementById("table");
			$("#table").find("tr:gt(0)").remove();
			for (var n = 0;n<VIds.length;n++){	
						row=table.insertRow();
						cell=row.insertCell();
						cell.innerHTML=VIds[n];
						cell=row.insertCell();
						cell.innerHTML=BIds[n];
						cell=row.insertCell();
						cell.innerHTML=directions[n];
						cell=row.insertCell();
						cell.innerHTML=destinations[n];
						cell=row.insertCell();
						if(delays[n]>120){cell.innerHTML=delays[n]-120;}
						else{cell.innerHTML="No Delay.";}
					}
				}	
			});
		}
