function fib(){
	
	var number=document.getElementById("number").value;
	if(isNaN(number)){
		document.getElementById("number").value="Invalid Input.";
	}
	else if (number<1){
		document.getElementById("number").value="Cannot comput Fib of a negative number.";
	}
	else{
		document.getElementById("number").value="Fibonacci series up to "+number+".";
		var results=[0,1]
		var table=document.getElementById("table");
		for (var i = table.rows.length - 1; i > 0; i--) {
            table.deleteRow(i);
        }
		for(var n=2;n<=number;n++){
			results.push(results[results.length-1]+results[results.length-2]);
		}
		for(var n=0;n<=number;n++){
					row=table.insertRow();
					cell=row.insertCell();
					cell.innerHTML=n
					cell=row.insertCell();
					cell.innerHTML=results[n];
		}	
		console.log(results);
	}



}

