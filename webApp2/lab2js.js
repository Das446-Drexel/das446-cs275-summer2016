function requestWeather(){
	var zip;
	var temps=[];
	var icons=[];
	var hours=[];
	var key = $("#key").val();
	var URL = "https://api.wunderground.com/api/"+
			key + "/geolookup/q/autoip.json";
   	$.ajax({
		type: "GET",
		url: URL,
		contentType:  "application/json; charset=utf-8",
		data: "{}",
		dataType: "jsonp",
		success: function(msg) {
			console.log(URL);
			var json = msg;
			zip = json.location.zip;
	URL="https://api.wunderground.com/api/"+key+"/hourly/q/"+zip+".json";
	   	$.ajax({
		type: "GET",
		url: URL,
		contentType:  "application/json; charset=utf-8",
		data: "{}",
		dataType: "jsonp",
		success: function(msg) {
			console.log(URL);
			var json = msg;
			for(var time in json.hourly_forecast){
				temps.push(json.hourly_forecast[time].temp.english);
				icons.push(json.hourly_forecast[time].icon_url);
				console.log(json.hourly_forecast[time].FCTTIME.pretty);
				hours.push(json.hourly_forecast[time].FCTTIME.pretty);
					};
			var table=document.getElementById("table");
			$("#table").find("tr:gt(0)").remove();
			for (var n = 0;n<$("#number").val();n++){	
							
						row=table.insertRow();
						cell=row.insertCell();
						cell.innerHTML=hours[n];
						cell=row.insertCell();
						cell.innerHTML="<img src='"+icons[n]+"'/>";
						cell=row.insertCell();
						cell.innerHTML=temps[n];
					}
				}	
			});
		}
	});
}
